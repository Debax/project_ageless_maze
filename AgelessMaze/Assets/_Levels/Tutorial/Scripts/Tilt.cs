﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilt : MonoBehaviour
{
    public bool movement;
    private float playerSpeedMult = 15;
    public bool isFlat = true;
    private Rigidbody rigid;
    public float speed;

    private void Start()
    {
        movement = true;
        rigid = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {

        Vector3 tilt = Input.acceleration;

        if (isFlat)
            tilt = Quaternion.Euler(90, 0, 0) * tilt;

        rigid.AddForce(tilt * speed);

        Debug.DrawRay(transform.position + Vector3.up, tilt, Color.cyan);
       
        //Input for Keyboard testing
        {
            /*+ M
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        transform.Translate(new Vector3(h ,0 ,v ));
        if (movement)
        gameObject.transform.Translate(Input.acceleration.x * playerSpeedMult * Time.deltaTime, 0, Input.acceleration.y * playerSpeedMult * Time.deltaTime);
        */
        }
        //
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Plate"))
        {
            movement = false;
        }
    }
}
